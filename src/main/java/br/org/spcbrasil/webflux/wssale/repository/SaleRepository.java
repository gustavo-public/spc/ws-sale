package br.org.spcbrasil.webflux.wssale.repository;

import br.org.spcbrasil.webflux.wssale.domain.Sale;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface SaleRepository  extends ReactiveMongoRepository<Sale,Long> {
}
