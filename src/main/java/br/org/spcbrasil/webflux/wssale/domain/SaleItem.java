package br.org.spcbrasil.webflux.wssale.domain;

import lombok.*;
import org.springframework.data.annotation.Id;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaleItem {

    @Id
    private String id;
    private String productId;
    private String productName;

    private Double quantity;
    private Double price;

}
