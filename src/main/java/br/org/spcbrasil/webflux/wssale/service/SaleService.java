package br.org.spcbrasil.webflux.wssale.service;

import br.org.spcbrasil.webflux.wssale.domain.Sale;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SaleService {

    Mono<Sale> create(Sale sale);

    Flux<Sale> findAll();

    Flux<Sale> findAllDetailed();

}
