FROM maven:3.6.1-jdk-11 as builder

ADD . /tmp

WORKDIR /tmp

RUN mvn clean package  && \
	chmod -R +x /tmp/target/*.jar

RUN jlink \
    --add-modules \
        java.base,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument \
    --compress 2 \
    --strip-debug \
    --no-header-files \
    --no-man-pages \
    --output /opt/java-minimal

FROM debian:stretch-slim

RUN mkdir -p /opt/java-minimal

ENV JAVA_HOME=/opt/java-minimal
ENV PATH="$PATH:$JAVA_HOME/bin"

RUN echo $PATH

COPY --from=builder $JAVA_HOME $JAVA_HOME
COPY --from=builder /tmp/target/*.jar /root/app.jar

EXPOSE 80

WORKDIR /root