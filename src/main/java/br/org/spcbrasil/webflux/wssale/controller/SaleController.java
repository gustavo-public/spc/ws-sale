package br.org.spcbrasil.webflux.wssale.controller;

import br.org.spcbrasil.webflux.wssale.domain.Sale;
import br.org.spcbrasil.webflux.wssale.service.SaleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "ws-sale/api/sale")
@RequiredArgsConstructor
public class SaleController {

    private final SaleService service;

    @PostMapping
    public Mono<Sale> create(@Valid @RequestBody(required = true) Sale body) {
        return service.create(body);
    }

    @GetMapping
    public Flux<Sale> findAll(){
        return service.findAll();
    }

    @GetMapping(value = "/detailed")
    public Flux<Sale> findAllDetailed(){
        return service.findAllDetailed();
    }
}
