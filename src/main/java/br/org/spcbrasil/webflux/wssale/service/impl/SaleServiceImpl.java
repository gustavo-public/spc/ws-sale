package br.org.spcbrasil.webflux.wssale.service.impl;

import br.org.spcbrasil.webflux.wssale.domain.Sale;
import br.org.spcbrasil.webflux.wssale.domain.dto.ProductDTO;
import br.org.spcbrasil.webflux.wssale.domain.dto.ShopperDTO;
import br.org.spcbrasil.webflux.wssale.repository.SaleRepository;
import br.org.spcbrasil.webflux.wssale.service.SaleService;
import br.org.spcbrasil.webflux.wssale.web.ProductWebClient;
import br.org.spcbrasil.webflux.wssale.web.ShopperWebClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SaleServiceImpl implements SaleService {

    private final SaleRepository repository;

    private final ProductWebClient productWebClient;
    private final ShopperWebClient shopperWebClient;

    @Override
    public Mono<Sale> create(Sale sale) {
        return Mono.just(sale)
                    .doOnNext(s -> s.setId(UUID.randomUUID().toString()))
                    .doOnNext(s -> s.getItems().forEach(i -> i.setId(UUID.randomUUID().toString())))
                    .doOnNext(s -> s.setCreatedAt(LocalDate.now()))
                    .flatMap(repository::save);
    }

    @Override
    public Flux<Sale> findAll() {
        return repository.findAll();
    }

    @Override
    public Flux<Sale> findAllDetailed() {
        return findAll()
                .flatMap(s -> Mono.just(s)
                                .map(Sale::getId)
                                .flatMap(shopperWebClient::getOneById)
                                .map(ShopperDTO::getName)
                                .doOnNext(s::setShopperName)
                                .map(name -> s)
                )
//      METODO 1 -
                .map(this::metodo1)
//      METODO 2 - sem subscribe
//                .doOnNext(this::metodo2)
//      METODO 3
//                .flatMap(this::metodo3)
                .onErrorMap(RuntimeException::new);
    }

   private Sale metodo1(Sale s){
       s.getItems()
        .forEach(item -> Mono.just(item)
                                .flatMap(i -> productWebClient.getOneById(i.getProductId())
                                                        .onErrorReturn(ProductDTO.builder()
                                                                                .name("Desconhecido")
                                                                                .build())// .subscribe()
                                )
                                .doOnNext(p -> item.setProductName(p.getName()))
       );
       return s;
   }

   private void metodo2(Sale s ){
        Flux.fromIterable(s.getItems())
            .doOnNext(item ->  Mono.just(item)
                                    .flatMap(i -> productWebClient.getOneById(i.getProductId())
                                                                    .onErrorReturn(ProductDTO.builder()
                                                                    .name("Desconhecido")
                                                                    .build())
                                    )
                                    .doOnNext(p -> item.setProductName(p.getName())).subscribe()
            );
   }

   private Mono<Sale>metodo3(Sale s){
        return  Flux.fromIterable(s.getItems())
                    .flatMap(item -> Mono.just(item)
                                        .flatMap(i -> productWebClient
                                                    .getOneById(i.getId())
                                                    .onErrorReturn(ProductDTO.builder()
                                                                                .name("Desconhecido")
                                                                                .build())
                                        )
                                        .map(ProductDTO::getName)
                                        .doOnNext(item::setProductName)
                                        .map(name -> item)
                    )
                    .map(i -> s)
                    .last();
   }

}
