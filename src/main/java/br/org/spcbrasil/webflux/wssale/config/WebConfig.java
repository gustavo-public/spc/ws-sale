package br.org.spcbrasil.webflux.wssale.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class WebConfig {

    @Value(value = "${ws-shopper.base-url}")
    private String wsShopperBaseUrl;

    @Value(value = "${ws-product.base-url}")
    private String wsProductBaseUrl;

    @Bean
    public WebClient wsShopperWebClient() {
        return WebClient.builder()
                .baseUrl(wsShopperBaseUrl)
                .filter(logRequest())
                .build();
    }

    @Bean
    public WebClient wsProductWebClient() {
        return WebClient.builder()
                .baseUrl(wsProductBaseUrl)
                .filter(logRequest())
                .build();
    }

    private static ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            log.info("Request: {} {}", clientRequest.method(), clientRequest.url());
            clientRequest.headers().forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
            return Mono.just(clientRequest);
        });
    }

}
