package br.org.spcbrasil.webflux.wssale.web;

import br.org.spcbrasil.webflux.wssale.domain.dto.ShopperDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ShopperWebClient {

    private final WebClient wsShopperWebClient;

    public Mono<ShopperDTO>getOneById(String shopperId){

        return wsShopperWebClient.get().attribute("id",shopperId)
                                    .exchangeToMono(res -> {
                                        if(res.statusCode().is2xxSuccessful())
                                            return res.bodyToMono(ShopperDTO.class);
                                        return Mono.error(new RuntimeException("Error: "+res.rawStatusCode()));
                                    });

    }

}
