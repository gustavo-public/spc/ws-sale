package br.org.spcbrasil.webflux.wssale.web;

import br.org.spcbrasil.webflux.wssale.domain.dto.ProductDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ProductWebClient {

    private final WebClient wsProductWebClient;

    public Mono<ProductDTO> getOneById(String productId){
        return wsProductWebClient.get()
                            .attribute("id",productId)
                            .exchangeToMono(res ->{
                                if(res.statusCode().is2xxSuccessful())
                                    return res.bodyToMono(ProductDTO.class);
                                return Mono.error(new RuntimeException("Error: "+res.rawStatusCode()));
                            });
    }

}
