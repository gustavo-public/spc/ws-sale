package br.org.spcbrasil.webflux.wssale.domain;


import lombok.*;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Sale {

    @Id
    private String id;
    private String shopperId;
    private String shopperName;

    private List<SaleItem>items;

    private LocalDate createdAt;

}
